﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SocketServer
{
    static class MessageSender
    {
        public static void SendMessage(Socket handler,string message)
        {
            byte[] byteMesage = Encoding.UTF8.GetBytes(message);
            handler.Send(byteMesage);
        }
    }
}
