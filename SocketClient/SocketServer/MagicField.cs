﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketServer
{
    public sealed class MagicField
    {
        public static String magicWord = String.Empty;
        public static string secret = String.Empty;


        public static void SetWord()
        {
            List<String> words = new List<String>();
            words.Add("UKRAINE");
            words.Add("BULGARIA");
            words.Add("POLAND");
            words.Add("FRANCE");

            Random rand = new Random();
            int index = rand.Next(0, 3);

            magicWord = words[index];
            secret = new String('*', magicWord.Length);
        }
    }
}
