﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SocketServer
{
    class Program
    {
        static void Main(string[] args)
        {
            IPHostEntry ipHost = Dns.GetHostEntry("localhost");
            IPAddress ipAddr = ipHost.AddressList[0];
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, 11000);

            Socket sListener = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                sListener.Bind(ipEndPoint);
                sListener.Listen(10);

                //ініціалізовуємо слова, які потрібно відгадати
                MagicField.SetWord();

                while (true)
                {
                    Socket handler = sListener.Accept();
                    string data = null;
                    
                    //надсилаємо слово, яке потрібно відгадати клієнту
                    MessageSender.SendMessage(handler, MagicField.secret);

                    byte[] bytes = new byte[1024];
                    int bytesRec = handler.Receive(bytes);

                    data += Encoding.UTF8.GetString(bytes, 0, bytesRec).ToUpper();

                    //користувач вгадує букву
                    if (data.Length ==1&&SecretChecker.CheckLetterInWord(data[0], MagicField.magicWord))
                    {
                        Player.Tries++;
                        Player.Score++;

                        if (!MagicField.secret.Contains("*"))
                        {
                            string win = $"{Player.PlayerName} WIN!\nScore : {Player.Score}\nTries : {Player.Tries}\n";
                            MessageSender.SendMessage(handler, win);
                            MagicField.SetWord();
                            Player.Tries = 0;
                            Player.Score = 0;
                        }
                        else
                        {
                            String trueLetter = $"Yes , {data} is present in word!";
                            MessageSender.SendMessage(handler, trueLetter);
                        }
                    }
                    //статистика
                    else if (data == "STATISTIC")
                    {
                        string statistic = $"Name : {Player.PlayerName}\nScore : {Player.Score}\nTries : {Player.Tries}\n";
                        MessageSender.SendMessage(handler, statistic);
                    }
                    //користувач вгадав слово
                    else if(data== MagicField.magicWord)
                    {
                        Player.Tries++;
                        Player.Score += 2;

                        string win = $"\nWord was : {MagicField.magicWord}\n{Player.PlayerName} WIN!\nScore : {Player.Score}\nTries : {Player.Tries}\n";
                        MagicField.SetWord();
                        MessageSender.SendMessage(handler, win);

                        Player.Tries = 0;
                        Player.Score = 0;
                    }
                    //зміна нікнейму
                    else if (data.Contains("|"))
                    {
                        Player.ChangeNickName(data.Trim('|'));
                        Console.WriteLine($"Nickname was changed for : {Player.PlayerName}");
                        string nickNameChanged = $"\nYour nickname now is : {Player.PlayerName}";
                        MessageSender.SendMessage(handler,nickNameChanged);
                    }
                    else
                    {
                        Player.Tries++;
                        Player.Score++;
                        String falseLetter = $"No , {data} epsent in word!";
                        MessageSender.SendMessage(handler, falseLetter);
                    }

                    Console.WriteLine("Данi вiд юзера : " + data + "\n");

                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                Console.ReadLine();
            }
        }

    }
}
