﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketServer
{
    static class SecretChecker
    {
        public static bool CheckLetterInWord(char el,string key)
        {
            bool isApsent = false;
            StringBuilder str = new StringBuilder(MagicField.secret);
            for (int i = 0; i < key.Length; i++)
            {
                if (key[i] == el)
                {
                    isApsent = true;
                    str[i] = el;
                }
            }
            MagicField.secret = str.ToString();

            return isApsent;
        }
    }
}
