﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketServer
{
    public static class Player
    {
        public static String PlayerName = "Player1";
        public static Int32 Score = 0;
        public static Int32 Tries = 0; 

        public static void ChangeNickName(String name)
        {
            PlayerName = name;
        }
    }
}
