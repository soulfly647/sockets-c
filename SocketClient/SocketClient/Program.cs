﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using SocketClient.Command;
using SocketClient.Command.Commands;

namespace SocketClient
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                SendMessageFromSocket(11000);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                Console.ReadLine();
            }
        }

        static void SendMessageFromSocket(int port)
        {
            byte[] bytes = new byte[1024];

            IPHostEntry ipHost = Dns.GetHostEntry("localhost");
            IPAddress ipAddr = ipHost.AddressList[0];
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, port);

            using (Socket sender = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp))
            {

                sender.Connect(ipEndPoint);

                //отримуємо зашифроване слово,яке нам потрібно вгадати
                int bytesRec = sender.Receive(bytes);

                String answer = Encoding.UTF8.GetString(bytes, 0, bytesRec);

                //точка повернення
                startPoint:

                Console.WriteLine("\nWord is : {0}\n", answer);

                Console.Write("What we have to do? Write command : ");
                string message = Console.ReadLine();

                Invoker invoker = new Invoker();
                ICommand command = invoker.GetCommand(message.ToLower(), sender);
                message = command.Execute();
                if(command is Other)
                    goto startPoint;


                byte[] msg = Encoding.UTF8.GetBytes(message);

                int bytesSent = sender.Send(msg);

                // Отримуємо відповідь від сервера
                int bytesRec2 = sender.Receive(bytes);
                String answer2 = Encoding.UTF8.GetString(bytes, 0, bytesRec2);

                //перевірка на виграш
                if (answer2.IndexOf("WIN") >= 0)
                {
                    Console.Clear();
                    Console.WriteLine("\nYou WIN!");
                    Console.WriteLine(answer2);
                    Console.ReadKey();
                    Console.Clear();
                    Console.WriteLine("New Game!\n");
                }
                else
                {
                    Console.WriteLine("\n" + answer2);
                }
                SendMessageFromSocket(port);
            }
        }
    }
}
