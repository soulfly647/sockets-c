﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SocketClient.Command.Commands
{
    class Exit: ICommand
    {
        public Socket Sender { get; set; }
        public string commandText { get; set; }

        public Exit()
        {
            commandText = "exit";
        }

        public string Execute()
        {
            Sender.Shutdown(SocketShutdown.Both);
            Sender.Close();
            Environment.Exit(0);
            return String.Empty;
        }
    }
}
