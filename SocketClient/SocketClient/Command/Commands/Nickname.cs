﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SocketClient.Command.Commands
{
    class Nickname: ICommand
    {
        public Socket Sender { get; set; }
        public string commandText { get; set; }

        public Nickname()
        {
            commandText = "nickname";
        }

        public string Execute()
        {
            Console.Clear();
            Console.WriteLine("Enter your new nickname : ");
            String message = '|' + Console.ReadLine();
            return message;
        }
    }
}
