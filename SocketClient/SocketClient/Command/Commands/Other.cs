﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SocketClient.Command.Commands
{
    class Other: ICommand
    {
        public Socket Sender { get; set; }
        public string commandText { get; set; }
        public string Execute()
        {
            Console.WriteLine($"Incorrect command!\nPlease Write correct command!");
            System.Threading.Thread.Sleep(3000);
            Console.Clear();

            return String.Empty;
        }
    }
}
