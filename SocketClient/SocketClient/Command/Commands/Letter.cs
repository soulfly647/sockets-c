﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SocketClient.Command.Commands
{
    class Letter: ICommand
    {
        public Socket Sender { get; set; }
        public string commandText { get; set; }

        public Letter()
        {
            commandText = "letter";
        }

        public string Execute()
        {
            Console.WriteLine("Write letter :");
            String message = Convert.ToString(Console.ReadKey().KeyChar);
            return message;
        }
    }
}
