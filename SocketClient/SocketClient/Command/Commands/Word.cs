﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SocketClient.Command.Commands
{
    class Word:ICommand
    {
        public Socket Sender { get; set; }
        public string commandText { get; set; }

        public Word()
        {
            commandText = "word";
        }

        public string Execute()
        {
            Console.WriteLine("Write word :");
            String message = Console.ReadLine();
            return message;
        }
    }
}
