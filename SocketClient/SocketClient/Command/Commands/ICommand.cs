﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SocketClient.Command
{
    interface ICommand
    {
        Socket Sender { get; set; }

        string commandText { get; set; }

        string Execute();
    }
}
