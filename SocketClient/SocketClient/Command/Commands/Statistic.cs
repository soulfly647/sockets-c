﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SocketClient.Command.Commands
{
    class Statistic: ICommand
    {
        public Socket Sender { get; set; }
        public string commandText { get; set; }

        public Statistic()
        {
            commandText = "statistic";
        }

        public string Execute()
        {
            String message = "statistic";
            return message;
        }
    }
}
