﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using SocketClient.Command.Commands;

namespace SocketClient.Command
{
    class Invoker
    {
        public Invoker()
        {
            commandList.Add(new Word());
            commandList.Add(new Letter());
            commandList.Add(new Statistic());
            commandList.Add(new Nickname());
            commandList.Add(new Exit());
        }

        public ICommand GetCommand(string message,Socket socket)
        {
            foreach (ICommand command in commandList)
            {
                ICommand commandObj = command;
                if (commandObj.commandText == message)
                {
                    commandObj.Sender = socket;
                    return commandObj;
                }
            }
            return new Other();
        }


        private ArrayList commandList = new ArrayList();
    }
}
